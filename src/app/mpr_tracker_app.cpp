#include <yaml-cpp/yaml.h>

#include <iomanip>

#include "srrg_boss/serializer.h"
#include "srrg_image_utils/depth_utils.h"
#include "srrg_messages/message_reader.h"
#include "srrg_messages/message_timestamp_synchronizer.h"
#include "srrg_messages/message_writer.h"
#include "srrg_messages/pinhole_image_message.h"
#include "srrg_messages/spherical_image_message.h"
#include "srrg_mpr/mpr_pyramid_generator.h"
#include "srrg_mpr/mpr_solver.h"
#include "srrg_mpr/mpr_tracker.h"
#include "srrg_mpr/mpr_utils.h"
#include "srrg_system_utils/system_utils.h"
#include "srrg_types/cloud_3d.h"

// uncomment for timing stats
#define __PROFILE__

using namespace std;
using namespace srrg_core;
using namespace srrg_mpr;
typedef std::map<
    std::string,
    MPRTracker::Config,
    std::less<std::string>,
    Eigen::aligned_allocator<std::pair<std::string, MPRTracker::Config> > >
    StringMPRTrackerConfigMap;

#include <math.h>
// VELODYNE64 has 2deg of positive rays and and 24.9deg of negative.
// this var serves as a row shifting
const float VELODYNE_POSITIVE_RAYS = 2.0 * M_PI / 180.0;
const float VELODYNE_IMAGE_CENTER_SHIFT = 11.45 * M_PI / 180.0;
// i.e. (26.9/2)-2, (velodyne fov/2 - positive rays)
//
Eigen::Isometry3f T_mpr2Kitti, T_mpr2Kitti_inv;

StringMPRTrackerConfigMap configs;

void initConfigs() {
  MPRTracker::Config c_base;

  c_base.pyramid.min_depth = .3;
  c_base.pyramid.max_depth = 10;

  MPRTracker::Config c_320x240 = c_base;
  c_320x240.pyramid.scales.push_back(std::make_pair(2, 2));
  c_320x240.levels_iterations.push_back(3);
  c_320x240.pyramid.scales.push_back(std::make_pair(4, 4));
  c_320x240.levels_iterations.push_back(30);

  configs.insert(std::make_pair("320x240", c_320x240));

  // MPRTracker::Config c_640x480=c_base;
  // c_640x480.pyramid.scales.push_back(std::make_pair(4,4));
  // c_640x480.levels_iterations.push_back(3);
  // c_640x480.pyramid.scales.push_back(std::make_pair(8,8));
  // c_640x480.levels_iterations.push_back(30);
  // configs.insert(std::make_pair("640x480", c_640x480));

  MPRTracker::Config c_640x480 = c_base;
  c_640x480.pyramid.scales.push_back(std::make_pair(8, 8));
  c_640x480.levels_iterations.push_back(20);
  c_640x480.pyramid.scales.push_back(std::make_pair(16, 16));
  c_640x480.levels_iterations.push_back(5);
  configs.insert(std::make_pair("640x480", c_640x480));

  MPRTracker::Config c_kinectv2 = c_base;
  c_kinectv2.pyramid.scales.push_back(std::make_pair(4, 4));
  c_kinectv2.levels_iterations.push_back(30);
  c_kinectv2.pyramid.scales.push_back(std::make_pair(8, 8));
  c_kinectv2.levels_iterations.push_back(30);
  c_kinectv2.solver.omega_intensity = 0.f;
  configs.insert(std::make_pair("kinectv2", c_kinectv2));

  MPRTracker::Config c_720x120_spherical;
  c_720x120_spherical.pyramid.camera_type = MPRPyramidLevel::Spherical;
  c_720x120_spherical.pyramid.normals_max_endpoint_distance = 1.f;
  c_720x120_spherical.pyramid.normals_cross_region_range_col = 2;
  c_720x120_spherical.pyramid.normals_cross_region_range_row = 2;
  c_720x120_spherical.pyramid.normals_blur_region_size = 2;
  c_720x120_spherical.pyramid.min_depth = 1.5f;
  c_720x120_spherical.pyramid.max_depth = 30.f;
  c_720x120_spherical.pyramid.scales.push_back(std::make_pair(2, 2));
  c_720x120_spherical.levels_iterations.push_back(100);
  c_720x120_spherical.solver.omega_intensity = 0.;  // default no intensity
  c_720x120_spherical.solver.depth_error_rejection_threshold = 3.f;
  c_720x120_spherical.solver.kernel_chi_threshold = .1;
  configs.insert(std::make_pair("720x120_spherical", c_720x120_spherical));

  MPRTracker::Config c_velodyne64;
  c_velodyne64.pyramid.camera_type = MPRPyramidLevel::Spherical;
  c_velodyne64.pyramid.normals_max_endpoint_distance = 15.f;
  c_velodyne64.pyramid.normals_cross_region_range_col = 1;
  c_velodyne64.pyramid.normals_cross_region_range_row = 2;
  c_velodyne64.pyramid.normals_scaled_blur_multiplier = 0;
  c_velodyne64.pyramid.normals_blur_region_size = 2;
  c_velodyne64.pyramid.min_depth = 1.5f;
  c_velodyne64.pyramid.max_depth = 120.f;
  c_velodyne64.pyramid.scales.push_back(std::make_pair(2, 2));
  c_velodyne64.levels_iterations.push_back(50);
  c_velodyne64.solver.omega_intensity = 0.;  // default no intensity
  c_velodyne64.solver.depth_error_rejection_threshold = 10.f;
  c_velodyne64.solver.kernel_chi_threshold = .1;
  configs.insert(std::make_pair("velodyne64", c_velodyne64));
}

void printConfigs() {
  cerr << "available configs" << endl;
  for (auto it : configs) { cerr << it.first << endl; }
  cerr << endl;
}
const char* banner[] = {
    "\n\nUsage:   "
    "\n",
    "Example:   ", "Options:\n", "------------------------------------------\n",
    "-t <string>                 depth image topic name, as in the txtio file",
    "-rgbt <string>              rgb image topic name, as in the txtio file",
    "-o <string>                 output dump file with odometry computed by "
    "mpr",
    "-eval<string>               output dump file with odometry in evaluation ",
    "format", "-show-images      <flag>     show processed images",
    "-stream-check     <flag>     enable stream checking on seq numbers",
    "-verbose          <flag>     if set it talks",
    "-use-odom         <flag>     use odometry if available",
    "-use-motion-model <flag>     use motion model",
    "-nkf              <flag>     suppress writing non keyframes",
    "-list-configs               displays the configs and terminates",
    "-config           <string>   selects a config from the list",
    "-omega-intensity  <float>    set omega for intensity channel (default "
    "1.f)",
    "-omega-depth      <float>    set omega for intensity channel (default "
    "1.f)",
    "-omega-normals    <float>    set omega for normals channel (default 1.f)",
    "-keyframe-trans   <float>    set the trans between keyframes (default "
    "0.1f)",
    "-keyframe-rot     <float>    set the rota between keyframes  (default "
    "0.1f)",
    "-omega-normals    <float>    set omega for normals channel (default 1.f)",
    "-h                          this help\n", 0};

int main(int argc, char** argv) {
  std::string depth_topic = "";
  std::string rgb_topic = "";
  std::string filename = "";
  std::string output_filename = "";
  std::string evaluation_filename = "";
  std::ofstream evaluation_file;
  bool evaluate = false;
  bool verbose = false;
  bool stream_check = false;
  bool use_odometry = false;
  bool use_motion_model = false;
  bool show_images = false;

  bool time_stats = false;
  std::string time_stats_filename;

  std::vector<std::string> depth_topics;
  std::vector<std::string> rgb_topics;
  float omega_intensity = 1.f;
  float omega_depth = 1.f;
  float omega_normals = 1.f;
  float keyframe_trans = 0.1f;
  float keyframe_rot = 0.1f;
  bool suppress_non_keyframes = false;

  std::string config_name = "640x480";

  initConfigs();

  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-list-configs")) {
      printConfigs();
      return 1;
    } else if (!strcmp(argv[c], "-t")) {
      c++;
      depth_topic = argv[c];
      depth_topics.push_back(depth_topic);
    } else if (!strcmp(argv[c], "-rgbt")) {
      c++;
      rgb_topic = argv[c];
      rgb_topics.push_back(rgb_topic);
    } else if (!strcmp(argv[c], "-o")) {
      c++;
      output_filename = argv[c];
    } else if (!strcmp(argv[c], "-eval")) {
      c++;
      evaluation_filename = argv[c];
      evaluate = true;
    } else if (!strcmp(argv[c], "-use-odom")) {
      use_odometry = true;
    } else if (!strcmp(argv[c], "-show-images")) {
      show_images = true;
    } else if (!strcmp(argv[c], "-use-motion-model")) {
      use_motion_model = true;
    } else if (!strcmp(argv[c], "-config")) {
      c++;
      config_name = argv[c];
    } else if (!strcmp(argv[c], "-omega-depth")) {
      c++;
      omega_depth = atof(argv[c]);
    } else if (!strcmp(argv[c], "-omega-intensity")) {
      c++;
      omega_intensity = atof(argv[c]);
    } else if (!strcmp(argv[c], "-omega-normals")) {
      c++;
      omega_normals = atof(argv[c]);
    } else if (!strcmp(argv[c], "-keyframe-trans")) {
      c++;
      keyframe_trans = atof(argv[c]);
    } else if (!strcmp(argv[c], "-keyframe-rot")) {
      c++;
      keyframe_rot = atof(argv[c]);
    } else if (!strcmp(argv[c], "-verbose")) {
      verbose = true;
    } else if (!strcmp(argv[c], "-nkf")) {
      suppress_non_keyframes = true;
    } else if (!strcmp(argv[c], "-stream-check")) {
      stream_check = true;
    } else if (!strcmp(argv[c], "-time-stats")) {
      time_stats = true;
      c++;
      time_stats_filename = argv[c];
    } else {
      filename = argv[c];
    }
    c++;
  }

  // Print Params
  cerr << "**** PARAMETERS ****\n";
  cerr << "depth-topics(-t)\t";
  for (int i = 0; i < depth_topics.size(); ++i) cerr << depth_topics[i] << " ";
  cerr << "\nrgb-topics(-rgbt)\t";
  for (int i = 0; i < rgb_topics.size(); ++i) cerr << rgb_topics[i] << " ";
  cerr << "\nconfig(-config)\t\t" << config_name;
  cerr << "\noutput file(-o)\t\t" << output_filename;
  if (evaluate) cerr << "\nevaluation file(-eval)\t" << evaluation_filename;
  if (use_odometry) cerr << "\nusing odometry on(-use-odom)";
  if (verbose) cerr << "\nverbose mode on(-verbose)";
  if (suppress_non_keyframes) cerr << "\nsuppress non keyframes on(-nkf)";
  if (stream_check) cerr << "\nstream check on(-stream-check)";
  cerr << "\nomega i(-omega-intensity)\t" << omega_intensity;
  cerr << "\nomega depth(-omega-depth)\t" << omega_depth;
  cerr << "\nomega normals(-omega-normals)\t" << omega_normals;
  cerr << "\nkeyframe translational threshold(-keyframe-trans)\t"
       << keyframe_trans;
  cerr << "\nkeyframe rotational threshold(-keyframe-rot)\t" << keyframe_rot;
  cerr << endl;

  // Axis rotation for kitti evaluation
  Eigen::Matrix3f R;
  // calib matrix from KITTI dataset (calib.txt)
  R << -1.857739385241e-03, -9.999659513510e-01, -8.039975204516e-03,
      -6.481465826011e-03, 8.051860151134e-03, -9.999466081774e-01,
      9.999773098287e-01, -1.805528627661e-03, -6.496203536139e-03;

  T_mpr2Kitti.setIdentity();
  T_mpr2Kitti.linear() = R;
  T_mpr2Kitti.translation() = Eigen::Vector3f(
      -4.784029760483e-03, -7.337429464231e-02, -3.339968064433e-01);
  T_mpr2Kitti_inv = T_mpr2Kitti.inverse();

  Eigen::Matrix3f image_center_correction = Eigen::Matrix3f(
      Eigen::AngleAxisf(VELODYNE_IMAGE_CENTER_SHIFT, Eigen::Vector3f::UnitZ()));
  Eigen::Isometry3f T_velodyne_image_center_shift;
  T_velodyne_image_center_shift.setIdentity();
  T_velodyne_image_center_shift.linear() = image_center_correction;

  Eigen::Isometry3f kitti_odom;
  kitti_odom.setIdentity();

  // store evaluation in TUM format
  if (evaluate) evaluation_file.open(evaluation_filename);

  bool has_rgb = (depth_topics.size() == rgb_topics.size());
  enum ImageType { Pinhole, Spherical };
  ImageType image_type = Pinhole;  // default

  std::vector<MessageTimestampSynchronizer> synchronizers(depth_topics.size());
  for (size_t i = 0; i < depth_topics.size(); i++) {
    std::vector<string> depth_plus_rgb_topic;
    depth_plus_rgb_topic.push_back(depth_topics[i]);
    if (has_rgb) depth_plus_rgb_topic.push_back(rgb_topics[i]);
    synchronizers[i].setTopics(depth_plus_rgb_topic);
    synchronizers[i].setTimeInterval(0.03);
  }

  MPRTracker tracker;
  tracker.setup();

  auto config_it = configs.find(config_name);
  if (config_it == configs.end()) {
    cerr << "config [" << config_name << "] not available" << endl;
    return 0;
  }

  std::vector<int> level_iterations;
  tracker.mutableConfig() = config_it->second;

  tracker.mutableConfig().solver.omega_intensity = omega_intensity;
  tracker.mutableConfig().solver.omega_depth = omega_depth;
  tracker.mutableConfig().solver.omega_normals = omega_normals;
  tracker.mutableConfig().keyframe_max_translation = keyframe_trans;
  tracker.mutableConfig().keyframe_max_rotation = keyframe_rot;
  tracker.mutableConfig().verbose = verbose;

  if (tracker.mutableConfig().pyramid.camera_type == MPRPyramidLevel::Spherical)
    image_type = Spherical;

  /// Stats
  int number_of_frames_current_window = 0;

  MessageReader reader;
  reader.open(filename);

  MessageWriter writer;
  bool writing = false;
  if (!output_filename.empty()) {
    writer.open(output_filename);
    writing = true;
  }

  // The transform
  Eigen::Isometry3f odom;
  odom.setIdentity();

  RGBImage rgb_image;
  RawDepthImage depth_image;

  int skip = 1;
  int count = 0;
  bool pyramid_init = false;

  double time_pyramid_cumulative = 0;
  double time_linearize_cumulative = 0;

  double time_projections_cumulative = 0;
  int max_dropped_frames = 3;

  int num_keyframes = 0;

  Eigen::Isometry3f previous_odom, motion_model_odom;
  previous_odom.setIdentity();
  motion_model_odom.setIdentity();

  std::vector<MPRStats, Eigen::aligned_allocator<MPRStats> > stats_history;
  int seq = -1;
  while (reader.good()) {
    BaseMessage* msg = reader.readMessage();
    if (!msg) { continue; }
    BaseImageMessage* base_img = dynamic_cast<BaseImageMessage*>(msg);
    if (!base_img) {
      delete msg;
      continue;
    }
    synchronizers[0].putMessage(base_img);
    if (!synchronizers[0].messagesReady()) { continue; }
    count++;
    if (count < skip) {
      synchronizers[0].reset();
      continue;
    }

    count = 0;
    PinholeImageMessage *depth_msg = 0, *rgb_msg = 0;
    SphericalImageMessage* spherical_msg = 0;

    int msg_seq;

    if (image_type == Spherical) {
      spherical_msg = dynamic_cast<SphericalImageMessage*>(
          synchronizers[0].messages()[0].get());
      msg_seq = spherical_msg->seq();
    } else {
      depth_msg = dynamic_cast<PinholeImageMessage*>(
          synchronizers[0].messages()[0].get());
      if (!depth_msg)
        throw std::runtime_error(
            "depth msg expected. Shall thou burn in hell.");
      msg_seq = depth_msg->seq();
      if (has_rgb)
        rgb_msg = dynamic_cast<PinholeImageMessage*>(
            synchronizers[0].messages()[1].get());
    }

    if (image_type == Pinhole) {
      //      if(!depth_msg || (has_rgb &&  !rgb_msg))) {
      if (!depth_msg) continue;
      if (has_rgb && !rgb_msg) continue;
    } else if (!spherical_msg) {
      continue;
    }

    if (stream_check && seq > -1 &&
        (msg_seq < seq + skip || msg_seq > seq + max_dropped_frames)) {
      cerr << "frame error detected" << endl;
      cerr << "expected seq: " << seq + 1 << " received seq: " << msg_seq;
      synchronizers[0].reset();
      continue;
    }
    seq = msg_seq;

    // camera matrix
    Eigen::Matrix3f K;
    // odometry, if any
    Eigen::Isometry3f odom;

    if (image_type == Pinhole) {
      // get camera matrix
      K = depth_msg->cameraMatrix();
      // get odometry
      odom = depth_msg->odometry();
      // get images
      depth_msg->image().copyTo(depth_image);
      if (has_rgb)
        rgb_msg->image().copyTo(rgb_image);
      else {
        rgb_image.create(depth_image.rows, depth_image.cols);
        rgb_image = cv::Vec3b(0, 0, 0);
      }
    } else {  // Spherical
      // get images
      spherical_msg->image().copyTo(depth_image);
      rgb_image.create(depth_image.rows, depth_image.cols);
      rgb_image = cv::Vec3b(0, 0, 0);
      // get camera matrix
      const Eigen::Vector4f spherical_K = spherical_msg->cameraMatrix();
      if (!strcmp(config_name.c_str(), "velodyne64")) {
        const float& vres = spherical_K(3);
        convertCameraMatrix(K, spherical_K, -VELODYNE_POSITIVE_RAYS * vres,
                            rgb_image.cols);  // velodyne
      } else {
        convertCameraMatrix(K, spherical_K, rgb_image.rows, rgb_image.cols);
      }
      // get odometry
      odom = spherical_msg->odometry();
    }

    double time_frame = getTime();

    if (!pyramid_init) {
      // set camera matrix, rows, cols
      const int rows = rgb_image.rows;
      const int cols = rgb_image.cols;
      cerr << "initializing pyramid: " << rows << "x" << cols << endl;
      tracker.mutableConfig().pyramid.rows = rows;
      tracker.mutableConfig().pyramid.cols = cols;
      tracker.mutableConfig().pyramid.camera_matrix = K;
      cerr << "rows: " << tracker.mutableConfig().pyramid.rows << endl;
      cerr << "cols: " << tracker.mutableConfig().pyramid.cols << endl;
      cerr << "K:    " << tracker.mutableConfig().pyramid.camera_matrix << endl;
      cerr << "pyramid initialized" << endl;
      pyramid_init = true;
    }

    tracker.setImages(depth_image, rgb_image);

    // //from lower resolution to higher
    std::ostringstream line_stream;
    line_stream
        << "**********************************************************\n";
    line_stream << "FRAME: " << number_of_frames_current_window << endl;

    if (use_odometry) {
      tracker.setRelativeMotionGuess(previous_odom.inverse() * odom);
    } else if (use_motion_model) {
      //      std::cerr << "mm: " << t2v(motion_model_odom).transpose() <<
      //      std::endl;
      tracker.setRelativeMotionGuess(motion_model_odom);
    }

    previous_odom = odom;
    Eigen::Isometry3f motion_model_old_odom = tracker.globalT();
    tracker.compute();
    odom = tracker.globalT();
    motion_model_odom = motion_model_old_odom.inverse() * odom;

    const MPRStats& stats = tracker.stats();
    if (stats.is_keyframe) { num_keyframes++; }
    line_stream << "keyframe num: " << num_keyframes << endl;

    for (int idx = stats.size() - 1; idx >= 0; --idx) {
      const MPRLevelStats& level_stats = stats[idx];
      const int num_iterations = level_stats.iterations.size();
      line_stream << "Level: "
                  << " { num: " << idx << ", "
                  << "iter: " << num_iterations << ", "
                  << "points: " << level_stats.cloud_size << ", ";
      const MPRIterationStats& start_iteration_stats =
          *level_stats.iterations.begin();
      const MPRIterationStats& end_iteration_stats =
          *level_stats.iterations.rbegin();
      line_stream << "chi: [" << start_iteration_stats.chi << ", "
                  << end_iteration_stats.chi << "], ";
      line_stream << "inliers: [" << start_iteration_stats.num_inliers << ", "
                  << end_iteration_stats.num_inliers << "] }\n";
    }
    line_stream << "Timings: { ";
    line_stream << "pyramid : " << stats.time_pyramid << ", ";
    line_stream << "project : " << stats.timeProjections() << ", ";
    line_stream << "linearize : " << stats.timeLinearize() << "}\n";

    time_pyramid_cumulative += stats.time_pyramid;
    time_projections_cumulative += stats.timeProjections();
    time_linearize_cumulative += stats.timeLinearize();

    line_stream << "**********************************************************"
                << endl;
    cerr << line_stream.str() << endl;
    stats_history.push_back(stats);

    ++number_of_frames_current_window;

    if (writing && (stats.is_keyframe || !suppress_non_keyframes)) {
      if (image_type == Pinhole) {
        depth_msg->setOdometry(odom);
        writer.writeMessage(*depth_msg);
        if (has_rgb) {
          rgb_msg->setOdometry(odom);
          writer.writeMessage(*rgb_msg);
        }
      } else {  // Spherical
        spherical_msg->setOdometry(odom);
        writer.writeMessage(*spherical_msg);
      }
    }
    if (evaluate) {
      if (image_type == Pinhole) {  // TUM style
        evaluation_file << std::setprecision(
                               std::numeric_limits<double>::digits10)
                        << rgb_msg->timestamp() << " ";
        evaluation_file << std::setprecision(
                               std::numeric_limits<float>::digits10)
                        << t2vFull(odom).transpose() << endl;
      } else {  // kitti style
        // first rotate the poses to be compliant with the reference frames (x
        // -> z and so on)
        // then apply the pitch rotation due to velodyne fov, then add the
        // relative motion
        kitti_odom =
            kitti_odom * (T_mpr2Kitti * motion_model_odom * T_mpr2Kitti_inv);
        Eigen::Isometry3f kitti_odom_image_shifted =
            T_velodyne_image_center_shift * kitti_odom;
        for (uint8_t r = 0; r < 3; ++r)
          for (uint8_t c = 0; c < 4; ++c)
            evaluation_file << kitti_odom_image_shifted(r, c) << " ";
        evaluation_file << endl;
        // std::cerr << "##########################################   z: "<<
        // kitti_odom(1,3) << std::endl;
      }
    }

    if (show_images) {
      if (image_type == Spherical)
        cv::imshow("Input depth/range", depth_image);
      else
        cv::imshow("Input depth/range",
                   depth_image * tracker.constConfig().pyramid.max_depth);

      if (has_rgb) cv::imshow("Input Intensity", rgb_image);
      cv::waitKey(1);
    }

    synchronizers[0].reset();
  }

#ifdef __PROFILE__
  float cumulative_time = time_linearize_cumulative +
                          time_projections_cumulative + time_pyramid_cumulative;
  float period = cumulative_time / (float)number_of_frames_current_window;
  std::cerr << "number of frames: " << number_of_frames_current_window << endl
            << endl;
  std::cerr << "cumulative pyramid time: " << time_pyramid_cumulative << endl;
  std::cerr << "cumulative projections time: " << time_projections_cumulative
            << endl;
  std::cerr << "cumulative linearize time: " << time_linearize_cumulative
            << endl;
  std::cerr << "cumulative time: " << cumulative_time << endl;
  std::cerr << "avg time per frame: " << period << " s" << endl;
  std::cerr << "frequency: " << 1. / period << " Hz" << endl;

  if (time_stats) {
    std::fstream fs;
    fs.open(time_stats_filename, std::fstream::app);
    fs << std::to_string(period) << endl;
    fs.close();
  }

#endif  // __PROFILE__

  if (writing) {
    cerr
        << "\nIf you have 'srrg_nicp_tracker_gui' installed, you can visualize "
           "the output of the mpr reconstruction typing:\n\n\t";
    cerr << "rosrun srrg_nicp_tracker_gui srrg_nicp_tracker_gui_app -config "
            "do_nothing -bpr 10 ";
    if (image_type == Spherical)
      cerr << "-aligner spherical -max_distance 120.0 ";
    cerr << "-t " << depth_topic << " ";
    if (image_type == Pinhole && has_rgb) cerr << "-rgbt " << rgb_topic << " ";
    cerr << output_filename << endl << endl;

    std::string stats_filename = output_filename + "_stats.boss";
    cerr << "Writing the stats on file [" << stats_filename << "]" << endl;
    srrg_boss::Serializer ser;
    ser.setFilePath(stats_filename);
    for (size_t i = 0; i < stats_history.size(); ++i) {
      ser.writeObject(stats_history[i]);
    }
  }

  return 0;
}

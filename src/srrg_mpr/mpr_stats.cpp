#include "mpr_stats.h"
namespace srrg_mpr {

void MPRIterationStats::serialize(ObjectData& data, IdContext& context) {
  data.setFloat("chi", chi);
  data.setInt("num_inliers", num_inliers);
  // T.matrix().toBOSS(data, "T" );
}

void MPRIterationStats::deserialize(ObjectData& data, IdContext& context) {
  chi = data.getFloat("chi");
  num_inliers = data.getInt("num_inliers");
  // T.matrix().fromBOSS(data, "T" );
}

void MPRLevelStats::serialize(ObjectData& data, IdContext& context) {
  data.setInt("rows", rows);
  data.setInt("cols", cols);
  data.setInt("cloud_size", cloud_size);
  data.setDouble("time_proj", time_projections);
  data.setDouble("time_line", time_linearize);
  ArrayData* adata = new ArrayData;
  for (size_t i = 0; i < iterations.size(); ++i) {
    ObjectData* odata = new ObjectData;
    iterations[i].serialize(*odata, context);
    adata->add(odata);
  }
  data.setField("iterations", adata);
}

void MPRLevelStats::deserialize(ObjectData& data, IdContext& context) {
  rows = data.getInt("rows");
  cols = data.getInt("cols");
  cloud_size = data.getInt("cloud_size");
  time_projections = data.getDouble("time_proj");
  time_linearize = data.getDouble("time_line");
  ArrayData* adata = dynamic_cast<ArrayData*>(data.getField("iterations"));
  iterations.resize(adata->size());
  for (size_t i = 0; i < adata->size(); ++i) {
    ObjectData& odata = dynamic_cast<ObjectData&>((*adata)[i]);
    iterations[i].deserialize(odata, context);
  }
}

double MPRStats::timeProjections() const {
  double t = 0;
  for (const_iterator it = begin(); it != end(); ++it) {
    t += it->time_projections;
  }
  return t;
}

double MPRStats::timeLinearize() const {
  double t = 0;
  for (const_iterator it = begin(); it != end(); ++it) {
    t += it->time_linearize;
  }
  return t;
}

void MPRStats::serialize(ObjectData& data, IdContext& context) {
  data.setBool("is_keyframe", is_keyframe);
  data.setDouble("time_pyr", time_pyramid);
  T.matrix().toBOSS(data, "T");
  global_T.matrix().toBOSS(data, "global_T");
  ArrayData* adata = new ArrayData;
  for (size_t i = 0; i < size(); ++i) {
    ObjectData* odata = new ObjectData;
    at(i).serialize(*odata, context);
    adata->add(odata);
  }
  data.setField("levels", adata);
}

void MPRStats::deserialize(ObjectData& data, IdContext& context) {
  is_keyframe = data.getBool("is_keyframe");
  time_pyramid = data.getDouble("time_pyr");
  T.matrix().fromBOSS(data, "T");
  global_T.matrix().fromBOSS(data, "global_T");
  ArrayData* adata = dynamic_cast<ArrayData*>(data.getField("levels"));
  resize(adata->size());
  for (size_t i = 0; i < adata->size(); ++i) {
    ObjectData& odata = dynamic_cast<ObjectData&>((*adata)[i]);
    at(i).deserialize(odata, context);
  }
}

BOSS_REGISTER_CLASS(MPRStats)
BOSS_REGISTER_CLASS(MPRLevelStats)
BOSS_REGISTER_CLASS(MPRIterationStats)
}
